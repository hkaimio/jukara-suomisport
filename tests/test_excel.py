# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>

from pathlib import Path
from person import Gender, Members, MembershipKind
from suomisport import add_practice_periods, add_licenses
from datetime import date


def test_practice_period_read():
    f = Path("tests/data/ilmoittautuneet_aikido_2024.xlsx")
    members = Members()
    add_practice_periods(members, f, date(2024, 12, 31))
    p = members.get_by_id(11111111)
    assert p
    assert p.surname == "Hassuttelija"
    assert p.birthdate == date(1974, 1, 31)

    assert len(p.practice_periods) == 1
    assert p.practice_periods[0].start_date == date(2024, 3, 2)
    assert p.practice_periods[0].end_date == date(2024, 12, 31)
    assert len(p.memberships) == 0

    p = members.get_by_id(22222222)
    assert p

    assert len(p.practice_periods) == 1
    assert p.practice_periods[0].start_date == date(2024, 3, 5)
    assert p.practice_periods[0].end_date == date(2024, 12, 31)
    assert len(p.memberships) == 1
    assert p.memberships[0].start_date == date(2024, 3, 5)
    assert p.memberships[0].year == 2024
    assert p.memberships[0].kind == MembershipKind.YEARLY

    p = members.get_by_id(33333333)
    assert p

    assert len(p.practice_periods) == 1
    assert p.practice_periods[0].start_date == date(2024, 2, 1)
    assert p.practice_periods[0].end_date == date(2024, 12, 31)
    assert len(p.memberships) == 0


def test_license_period_read():
    f = Path("tests/data/lisenssit.xlsx")
    members = Members()
    add_licenses(members, f)
    p = members.get_by_id(11111111)
    assert p
    assert p.surname == "Hassuttelija"
    assert p.birthdate == date(1974, 1, 31)
    assert p.gender == Gender.MALE

    assert len(p.licenses) == 1
    assert p.licenses[0].start == date(2024, 3, 4)
    assert p.licenses[0].year == 2024
