# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>

from datetime import date
from pathlib import Path
from person import Members, MembershipKind
from helpers import date_from_datetime, read_excel_sheet

"""Importing data from other sources tahn Suomisport"""


def load_payments(members: Members, f: Path, year=2024):
    """Load payment information from an Excel sheet.

    Data is expected to be in the format used in Jukara for recording payments.

    Args:
        members (Members): The members object to update.
        f (Path): The path to the Excel file.
        year (int, optional): The year to load the data for. Defaults to 2024.

    Returns:
        None

    """
    columns = [
        ("Maksupäivä", date_from_datetime),
        ("Palvelu", None),
        ("Sukunimi", None),
        ("Etunimi", None),
        ("Sportti-ID", int),
        ("Jäsenmaksu", None),
        ("Harjoittelumaksu", None),
        ("Laji", None),
        ("Kirjattu Suomisportiin", None),
    ]

    rows = read_excel_sheet(f, year, columns)
    for row in rows:
        sport_id = row[4]
        sport = row[7]
        if not sport_id or sport != "Aikido":
            continue
        p = members.get_by_id(row[4])
        if not p:
            print(f"{row[3]} {row[2]} (soprtti-id {row[4]}) not found")
            continue
        membership = row[5]
        practice = row[6]
        d = row[0]
        service = row[1]
        if membership == "Kyllä":
            p.add_membership(year, MembershipKind.YEARLY, d, service)
        if practice == "Aikido koko vuosi":
            p.add_practice_period(d, date(year, 12, 31), service)
        elif practice == "Aikido kevät":
            p.add_practice_period(d, date(year, 6, 30), service)
        elif practice == "Aikido syksy":
            p.add_practice_period(d, date(year, 12, 31), service)
