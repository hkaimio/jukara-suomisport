# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>

from pathlib import Path
import os
import shutil
from typing import Any, Callable, Dict, Optional
from person import Gender, Members, MembershipKind
from datetime import date
from selenium import webdriver
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from helpers import (
    date_from_datetime,
    date_from_fin_fmt,
    get_excel_row_fields,
)
import time


def get_latest_downloaded():
    home_path = Path(os.environ["HOMEPATH"])
    download_dir = home_path / "Downloads"

    last_mtime = 0
    latest_file = None
    for f in download_dir.glob("*"):
        mtime = f.stat().st_mtime
        if mtime > last_mtime:
            latest_file = f
            last_mtime = mtime
    return latest_file


def copy_latest_downloaded(path: Path):
    latest_file = get_latest_downloaded()
    shutil.copy(latest_file, path)


class ElementFinder(object):
    def __init__(self, by: str, value: str):
        self._by = by
        self._value = value

    def __call__(self, driver: WebDriver):
        els = driver.find_elements(self._by, self._value)
        if len(els):
            return els[0]
        return False


def convert_field(val: str, f: Optional[Callable[[str], Any]]):
    return f(val) if f else val


def period_includes_membership_fee(period_desc: str):
    return period_desc.find("ja jäsenmaksu") >= 0


def add_practice_periods(members: Members, f: Path, end_date: date):
    import openpyxl

    wb = openpyxl.open(f)
    sheet = wb.active
    col_titles: Dict[str, int] = dict()
    for col in range(30):
        cell = sheet.cell(row=1, column=col + 1)
        if cell.value:
            col_titles[cell.value] = col + 1
        else:
            break

    person_field_defs = [
        ("Sportti-ID", int),
        ("Etunimi", None),
        ("Sukunimi", None),
        ("Syntymäaika", date_from_fin_fmt),
        ("Sukupuoli", None),
        ("Sähköposti", None),
        ("Puhelin", None),
    ]

    period_field_defs = [
        ("Ilmoittautumispvm", date_from_datetime),
        ("Valitun hintatyypin nimi", period_includes_membership_fee),
    ]

    r = 2
    while sheet.cell(row=r, column=1).value:
        p = members.update_or_add_person(
            *get_excel_row_fields(sheet, col_titles, r, person_field_defs)
        )
        period_info = get_excel_row_fields(sheet, col_titles, r, period_field_defs)
        period_start_date: date = period_info[0]
        p.add_practice_period(period_start_date, end_date, in_suomisport=True)
        if period_info[1]:
            # Assume that if training period fee includes a yearly membership fee it is for the year when the period ends
            p.add_membership(end_date.year, MembershipKind.YEARLY, period_start_date)
        r += 1


def add_memberships(members: Members, f: Path, year: int):
    import openpyxl

    wb = openpyxl.open(f)
    sheet = wb.active
    col_titles: Dict[str, int] = dict()
    for col in range(30):
        cell = sheet.cell(row=1, column=col + 1)
        if cell.value:
            col_titles[cell.value] = col + 1
        else:
            break

    person_field_defs = [
        ("Sportti-ID", int),
        ("Etunimi", None),
        ("Sukunimi", None),
        ("Syntymäaika", date_from_fin_fmt),
        ("Sukupuoli", None),
        ("Sähköposti", None),
        ("Puhelin", None),
    ]

    period_field_defs = [
        ("Ilmoittautumispvm", date_from_datetime),
    ]

    r = 2
    while sheet.cell(row=r, column=1).value:
        p = members.update_or_add_person(
            *get_excel_row_fields(sheet, col_titles, r, person_field_defs)
        )
        period_info = get_excel_row_fields(sheet, col_titles, r, period_field_defs)
        period_start_date: date = period_info[0]
        p.add_membership(year, MembershipKind.YEARLY, period_start_date, "Suomisport")
        r += 1


def add_licenses(members: Members, f: Path):
    import openpyxl

    wb = openpyxl.open(f)
    sheet = wb.active
    col_titles: Dict[str, int] = dict()
    for col in range(30):
        cell = sheet.cell(row=1, column=col + 1)
        if cell.value:
            col_titles[cell.value] = col + 1
        else:
            break

    person_field_defs = [
        ("Sportti-ID", int),
        ("Etunimi", None),
        ("Sukunimi", None),
        ("Syntymäaika", date_from_fin_fmt),
        ("Sukupuoli", Gender.FromFinnish),
        ("Sähköposti", None),
        ("Puhelin", None),
    ]

    license_field_defs = [
        ("Ostopäivä", date_from_fin_fmt),
    ]

    r = 2
    while sheet.cell(row=r, column=1).value:
        p = members.update_or_add_person(
            *get_excel_row_fields(sheet, col_titles, r, person_field_defs)
        )
        license_info = get_excel_row_fields(sheet, col_titles, r, license_field_defs)
        license_start_date: date = license_info[0]
        p.add_license(license_start_date)
        r += 1


def add_members(members: Members, f: Path):
    import openpyxl

    wb = openpyxl.open(f)
    sheet = wb.active
    col_titles: Dict[str, int] = dict()
    for col in range(30):
        cell = sheet.cell(row=1, column=col + 1)
        if cell.value:
            col_titles[cell.value] = col + 1
        else:
            break

    person_field_defs = [
        ("Sportti-id", int),
        ("Etunimi", None),
        ("Sukunimi", None),
        ("Sähköposti", None),
        ("Puhelin", None),
        ("Jäsenluokka", None),
    ]

    r = 2
    while sheet.cell(row=r, column=1).value:
        sport_id, first_name, surname, email, tel, member_class = get_excel_row_fields(
            sheet, col_titles, r, person_field_defs
        )
        p = members.update_or_add_person(
            sport_id, first_name, surname, None, Gender.NOT_KNOWN, email, tel
        )
        p.membership_class = member_class
        r += 1


class ScrapingException(Exception):
    pass


class PageHasLink(object):
    def __init__(self, check_link: Callable[[str, str], bool]):
        self._check_link = check_link
        pass

    def __call__(self, driver: WebDriver):
        links = driver.find_elements(By.TAG_NAME, "a")  # Finding the referenced element
        for link in links:
            url = link.get_attribute("href")
            if url and self._check_link(url, link.text):
                return link

        return False


class DataScraper(object):
    def __init__(
        self, master_club: int, federation: int, club: int, user: str, passwd: str
    ):
        self._master_club = master_club
        self._federation = federation
        self._club = club
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference(
            "browser.helperApps.neverAsk.saveToDisk", "application/octet-stream"
        )
        options = webdriver.FirefoxOptions()
        options.profile = profile
        self._driver = webdriver.Firefox(options=options)
        self._login(user, passwd)

    def _login(self, user: str, passwd: str):
        self._driver.get("https://www.suomisport.fi/?locale=fi_FI")
        wait = WebDriverWait(self._driver, 10)
        wait.until(ElementFinder(By.CLASS_NAME, "btn-lg"))
        elems = self._driver.find_elements(By.CLASS_NAME, "btn-lg")
        org_btn = None
        for elem in elems:
            if "organisaatiokäyttäjä" in elem.text:
                org_btn = elem

        if org_btn:
            org_btn.click()
        else:
            raise ScrapingException("No org login option found")

        # Login
        username_field = self._driver.find_element(By.ID, "usernameinput")
        username_field.clear()
        username_field.send_keys(user)

        pw_field = self._driver.find_element(By.ID, "passwordinput")
        pw_field.clear()
        pw_field.send_keys(passwd)

        btn_lg = self._driver.find_element(By.CLASS_NAME, "btn-lg")
        btn_lg.click()
        time.sleep(2)

    @property
    def main_page(self):
        """URL of the club main page."""
        return (
            f"https://www.suomisport.fi/administration/master_club/{self._master_club}"
        )

    def get_licenses(self):
        print(f"Opening page {self.main_page}")
        self._driver.get(self.main_page)

        wait = WebDriverWait(self._driver, 20)
        license_link = None
        while not license_link:
            try:
                license_link = wait.until(
                    PageHasLink(lambda url, _: url.endswith("licencedathlete"))
                )
            except:  # noqa: E722
                print(
                    f"Error logging in. Please log in manually and go to your club's main page ({self.main_page})"
                )

        if license_link:
            license_url = license_link.get_attribute("href")
            if not license_url:
                raise ScrapingException("No URL found lin license element")
        else:
            raise ScrapingException("No license link found")

        # Download licenses
        self._driver.get(license_url)

        def has_download_link(driver: WebDriver):
            links = driver.find_elements(By.TAG_NAME, "a")
            for link in links:
                if "Excel" in link.text:
                    return link
            return False

        wait = WebDriverWait(self._driver, 10)
        download_link = wait.until(has_download_link)
        download_link.click()

        time.sleep(5)
        license_file = get_latest_downloaded()
        return license_file

    def get_event_participants(self, event: int):
        url = f"https://www.suomisport.fi/administration/master_club/{self._master_club}/sportsfederations/{self._federation}/club/{self._club}/events/track/{event}"
        self._driver.get(url)
        wait = WebDriverWait(self._driver, 10)
        export_menu_item = wait.until(ElementFinder(By.ID, "exporticonwrapper"))
        export_menu_item.click()
        menu_links_item = wait.until(ElementFinder(By.CLASS_NAME, "eventmenulinks"))
        download_item = menu_links_item.find_element(By.TAG_NAME, "a")
        download_item.click()
        time.sleep(5)
        return get_latest_downloaded()

    def get_members(self):
        print(f"Opening page {self.main_page}")
        self._driver.get(self.main_page)

        wait = WebDriverWait(self._driver, 20)
        members_link = wait.until(PageHasLink(lambda url, _: url.endswith("/members")))
        if members_link:
            members_url = members_link.get_attribute("href")
            if not members_url:
                raise ScrapingException("No URL found in members element")
        else:
            raise ScrapingException("No members link found")

        # Download licenses
        self._driver.get(members_url)

        wait = WebDriverWait(self._driver, 10)

        member_download_menu = wait.until(ElementFinder(By.CLASS_NAME, "dropdown"))
        member_download_menu.click()
        download_link = wait.until(
            PageHasLink(lambda url, _: url.endswith("/export-members"))
        )
        download_link.click()

        time.sleep(5)
        license_file = get_latest_downloaded()
        return license_file
