# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>

from datetime import date, datetime
from pathlib import Path
import re
from typing import Any, Dict, List, Tuple
import openpyxl


def date_from_isofmt(s: str) -> date:
    """Parse a date string in ISO format and return a date object.

    Parameters:
    s (str): The date string in ISO format, e.g., "2023-02-14".

    Returns:
    date: The date object.

    Raises:
    ValueError: If the input string is not in the correct format.
    """
    date_pattern = r"^([0-9]{4})-([0-9]{2})-([0-9]{2})"
    m = re.match(date_pattern, s)
    if m:
        (year, month, day) = m.groups()
        return date(int(year), int(month), int(day))
    raise ValueError(f"Incorrect date string {s}")


def date_from_datetime(t: datetime) -> date:
    """Parse a datetime object and return a date object.

    Parameters:
    t (datetime): The datetime object.

    Returns:
    date: The date object.

    Raises:
    ValueError: If the input datetime object is not valid.
    """
    if not isinstance(t, datetime):
        raise ValueError(f"Expected a datetime object, got {type(t)}")
    return date(t.year, t.month, t.day)


def date_from_fin_fmt(s: str) -> date:
    """
    Parse a date string in Finnish date format (d.m.y) and return a date object.

    Parameters:
    s (str): The date string in Finnish format, e.g., "12.02.2023".

    Returns:
    date: The date object.

    Raises:
    ValueError: If the input string is not in the correct format.
    """
    date_pattern = r"^([0-9]+)\.([0-9]+).([0-9]+)"
    m = re.match(date_pattern, s)
    if m:
        (day, month, year) = m.groups()
        return date(int(year), int(month), int(day))
    raise ValueError(f"Incorrect date string {s}")


def get_excel_row_fields(
    sheet: openpyxl.worksheet.worksheet.Worksheet,
    columns: Dict[str, int],
    row: int,
    field_defs: List[Tuple[str, Any]],
):
    """Extracts values from a specific row in an Excel sheet based on the field definitions.

    Parameters:
    sheet (openpyxl.worksheet.worksheet.Worksheet): The Excel sheet.
    columns (Dict[str, int]): A mapping of field names to column indices.
    row (int): The row index.
    field_defs (List[Tuple[str, Any]]): A list of tuples, where each tuple represents
                                        a field definition, consisting of the field name
                                        and a transformation function (if applicable). If
                                        the function is provided, the value in Excel cell
                                        is processed by it.

    Returns:
    List[Any]: A list of field values.

    Raises:
    ValueError: If the input parameters are invalid.
    """
    if not isinstance(sheet, openpyxl.worksheet.worksheet.Worksheet):
        raise ValueError("Invalid sheet object")
    if not isinstance(columns, dict):
        raise ValueError("Invalid columns object")
    if not isinstance(row, int):
        raise ValueError("Invalid row index")
    if not isinstance(field_defs, list):
        raise ValueError("Invalid field definitions")

    fields = []
    for f in field_defs:
        val = sheet.cell(row=row, column=columns[f[0]]).value
        if val and f[1]:
            val = f[1](val)
        fields.append(val)

    return fields


def read_excel_sheet(f: Path, year: int, columns: List[Tuple[str, Any]]):
    """Reads an Excel sheet and returns the data as a list of lists.

    Parameters:
    f (Path): The path to the Excel file.
    year (int): Year of the payments
    columns (List[Tuple[str, Any]]): A list of tuples, where each tuple represents
                                     a column, consisting of the column name and a
                                     transformation function (if applicable).

    Returns:
    List[List[Any]]: A list of lists, where each sub-list represents a row of data.

    Raises:
    ValueError: If the input parameters are invalid.
    """
    if not isinstance(f, Path):
        print(f"{f} is {type(f)}")
        raise ValueError("Invalid file path")
    if not isinstance(columns, list):
        raise ValueError("Invalid column definitions")

    wb = openpyxl.open(f, data_only=True)
    sheet = wb[f"Liikuntaetumaksut {year}"]
    col_titles: Dict[str, int] = dict()
    for col in range(30):
        cell = sheet.cell(row=1, column=col + 1)
        if cell.value:
            col_titles[cell.value] = col + 1
        else:
            break

    r = 2
    ret: List[List[Any]] = list()
    while sheet.cell(row=r, column=1).value:
        ret.append(get_excel_row_fields(sheet, col_titles, r, columns))
        r += 1

    return ret
