# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>

from datetime import date
from pathlib import Path
import sys
from typing import List
from person import Members, Person
from ruamel.yaml import YAML
from suomisport import (
    DataScraper,
    add_licenses,
    add_memberships,
    add_practice_periods,
    add_members,
)
from manual_data import load_payments
import pickle
import click


def load_data(conf_file: Path):
    """Scrape data from Suomisport

    Args:
        conf_file (Path): Path to the configuration file.

    Returns:
        Members: The loaded data.
    """
    yaml = YAML()
    with open(conf_file) as f:
        conf = yaml.load(f)

    suomisport_conf = conf["suomisport"]
    club_conf = conf["club"]
    members = Members()

    scraper = DataScraper(
        club_conf["master_id"],
        club_conf["fed_id"],
        club_conf["club_id"],
        suomisport_conf["user"],
        suomisport_conf["passwd"],
    )

    license_file = scraper.get_licenses()
    if not license_file:
        print("No license file downloaded")
        sys.exit(-1)

    add_licenses(members, license_file)

    for period in club_conf["practice_period_events"]:
        event_id = period["event_id"]
        end_date = period["end_date"]
        participant_file = scraper.get_event_participants(event_id)
        if participant_file:
            add_practice_periods(members, participant_file, end_date)
        else:
            print(f"No file downloaded for event {event_id}")
            sys.exit(-1)

    for period in club_conf["membership_events"]:
        membership_event = period["event_id"]
        membership_year = period["year"]
        membership_payment_file = scraper.get_event_participants(membership_event)
        if membership_payment_file:
            add_memberships(members, membership_payment_file, membership_year)

    member_file = scraper.get_members()
    if member_file:
        add_members(members, member_file)
    return members


def bool_to_desc(v: bool):
    return "OK" if v else "PUUTTUU!"


def check_and_print_summary(members: Members):
    """Print a summary of the data in the given members object.

    Args:
        members (Members): The members object to summarize.

    Returns:
        None
    """
    person_count: int = 0
    license_count: int = 0
    member_count: int = 0
    practicer_count: int = 0
    not_active: List[Person] = list()
    no_issues: List[Person] = list()
    for p in members._persons.values():
        is_member = p.is_member(date.today())
        current_practice_period = p.get_active_practice_period(date.today())

        is_practicing = current_practice_period is not None
        practice_period_registered = is_practicing and (
            current_practice_period.in_suomisport
        )

        has_license = p.has_license(date.today())
        if not any((is_member, is_practicing, has_license)):
            not_active.append(p)
            continue

        membership_class_ok = (not is_member) or (
            p.membership_class in ["Ainaisjäsen", f"Jäsen {date.today().year}"]
        )

        person_count += 1
        license_count += 1 if has_license else 0
        member_count += 1 if is_member else 0
        practicer_count += 1 if is_practicing else 0

        if all(
            (membership_class_ok, is_member, practice_period_registered, has_license)
        ):
            no_issues.append(p)
        else:
            print(f"{p.first_name} {p.surname} {p.sportti_id}")
            print(f"  Lisenssi:       {bool_to_desc(has_license)}")
            print(f"  Jäsen:          {bool_to_desc(is_member)}")
            if not membership_class_ok:
                print(f"  Väärä jäsenluokka {p.membership_class}")

            practice_payment_message = bool_to_desc(is_practicing)
            if is_practicing and not practice_period_registered:
                practice_payment_message += " , ei kirjattu Suomisportiin"

            print(f"  Harjoitusmaksu: {practice_payment_message}")

    print(f"Yhteensä {person_count}")
    print(f"Lisenssejä {license_count}")
    print(f"Jäsenmaksuja {member_count}")
    print(f"Harjoitusmaksuja {practicer_count}")
    print(f"Ei-aktiivisia: {len(not_active)}")


@click.command()
@click.option("--scrape", is_flag=True, help="Scrape data from Suomisport web site")
@click.option("--conf", type=click.Path(), help="Scraping configuration file")
@click.option(
    "--data",
    type=click.Path(path_type=Path),
    help="Local data file. When scraping, the data is written in the file; otherwise, existing data is read from it.",
)
@click.option(
    "--ext-payments",
    type=click.Path(path_type=Path),
    help="Excel file with information abotu payments done elsewhere",
)
def command(scrape: bool, conf: Path, data: Path, ext_payments: Path):
    save_data = False
    if scrape:
        members = load_data(conf)
        if data:
            save_data = True

    else:
        if not data:
            print("Either --scrape or --data must be present")
            with click.Context(command) as ctx:
                click.echo(command.get_help(ctx))
                sys.exit(1)

        with open(data, "rb") as f:
            members = pickle.load(f)
    if ext_payments:
        load_payments(members, ext_payments, date.today().year)
        save_data = True

    if save_data:
        with open(data, "wb") as f:
            pickle.dump(members, f)

    check_and_print_summary(members)


if __name__ == "__main__":
    command()
