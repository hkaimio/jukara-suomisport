# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>

from enum import Enum
from dataclasses import dataclass, field
from datetime import date
from typing import Dict, List, Optional


class MemberType(Enum):
    MEMBER = 1
    PERMANENT = 2
    NOT_MEMBER = 3


class PaymentType(Enum):
    FULL_YEAR = 1
    HALF_YEAR = 2
    FAMILY_FULL_YEAR = 3
    FAMILY_HALF_YEAR = 4
    MONTHLY = 5
    UNKNOWN = 6


class Gender(Enum):
    MALE = 1
    FEMALE = 2
    OTHER = 3
    NOT_KNOWN = 4

    @staticmethod
    def FromFinnish(s: str):
        if s.lower() == "mies":
            return Gender.MALE
        if s.lower() == "nainen":
            return Gender.FEMALE
        if s.lower() == "muu":
            return Gender.OTHER
        raise ValueError(f"Unknown Finnish gender word {s}")

    @staticmethod
    def FromEnglish(s: str):
        if s.lower() == "male":
            return Gender.MALE
        if s.lower() == "female":
            return Gender.FEMALE
        if s.lower() == "other":
            return Gender.OTHER
        raise ValueError(f"Unknown English gender word {s}")


@dataclass
class License(object):
    year: int
    start: date


class MembershipKind(Enum):
    YEARLY = 1
    PERMANENT = 2


@dataclass
class Membership(object):
    year: int
    kind: MembershipKind
    start_date: date
    paid_in: Optional[str] = field(default=None)

    def is_valid(self, d: date):
        if self.kind == MembershipKind.PERMANENT:
            return d >= self.start_date
        else:
            return d >= self.start_date and d.year == self.year


@dataclass
class PracticePeriod(object):
    start_date: date
    end_date: date
    paid_in: Optional[str] = field(default=None)
    in_suomisport: Optional[bool] = field(default=False)

    def includes(self, d: date):
        return d >= self.start_date and d <= self.end_date


@dataclass
class Person(object):
    sportti_id: str
    surname: str
    first_name: str
    birthdate: date
    gender: Gender
    email: str
    phone: str
    parent_email: str
    parent_phone: str
    in_aikido_group: bool
    licenses: List[License] = field(default_factory=list)
    memberships: List[Membership] = field(default_factory=list)
    practice_periods: List[PracticePeriod] = field(default_factory=list)
    membership_class: Optional[str] = field(default=None)

    def add_membership(
        self, year: int, kind: MembershipKind, d: date, paid_in: Optional[str] = None
    ):
        for m in self.memberships:
            if m.year == year:
                m.kind = kind
                return

        if d.year < year:
            d = date.fromisocalendar(year, 1, 1)
        self.memberships.append(Membership(year, kind, d, paid_in))

    def add_practice_period(
        self,
        start: date,
        end: date,
        paid_in: Optional[str] = None,
        in_suomisport: Optional[bool] = False,
    ):
        exists = False
        for period in self.practice_periods:
            if period.end_date == end:
                period.paid_in = paid_in
                exists = True
                break

        if not exists:
            self.practice_periods.append(
                PracticePeriod(start, end, paid_in, in_suomisport)
            )

    def add_license(self, start: date):
        year = start.year
        for lic in self.licenses:
            if lic.year == year:
                if lic.start > start:
                    lic.start = start
                return
        self.licenses.append(License(year, start))

    def is_member(self, d: date):
        for m in self.memberships:
            if m.is_valid(d):
                return True

        if self.membership_class == "Ainaisjäsen":
            return True

        return False

    def is_practicing(self, d: date):
        for period in self.practice_periods:
            if d >= period.start_date and d <= period.end_date:
                return True

        return False

    def get_active_practice_period(self, d: date):
        for period in self.practice_periods:
            if d >= period.start_date and d <= period.end_date:
                return period

        return None

    def has_license(self, d: date):
        for license in self.licenses:
            if d >= license.start and d.year == license.year:
                return True

        return False


class Members(object):
    def __init__(self):
        self._persons: Dict[str, Person] = dict()

    def get_by_id(self, id: str) -> Optional[Person]:
        return self._persons.get(id, None)

    def update_or_add_person(
        self,
        sport_id: str,
        first: str,
        surname: str,
        birthdate: date,
        gender: Gender,
        email: str,
        phone: str,
    ):
        if sport_id not in self._persons:
            p = Person(
                sport_id, surname, first, birthdate, gender, email, phone, "", "", False
            )
            self._persons[sport_id] = p
        else:
            p = self._persons[sport_id]

        return p

    def reset(self):
        self._persons = dict()
