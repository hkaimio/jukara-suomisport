..
  SPDX-License-Identifier: CC-BY-4.0
  SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>

Suomisportin käyttö Jukaran aikidojaoksessa
===========================================

Tässä dokumentissa kuvataan Suomisportin seurapalvelujen käyttö Jukaran
aikidojaoksen toiminnassa. Aikidojaos käyttää palvelun jäsenrekisteriä sekä
tapahtumoat-osiota. Ryhmät-osion käytöstä on luovuttu, koska sen todettiin
soveltuvan huonosti aikidoharrastukseen, jossa ei ole selkeitä, koko kauden
samalla kokoonpanolla harrastavia ryhmiä. Sen sijaan harjoitus- ja jäsenmaksujen
maksaminen tapahtuu tätä varten luotujen tapahtumien kautta.

Jäsenrekisteri
--------------

Jäsenreisterissä pidetään kirjaa Jukaran jäsenistä sekä maksetuista
jäsenmaksuista. Seuraan liittyvän henkilön on tehtävä Suomisportissa
jäsenhakemus seuran verkkosivuilla olevan linkin kautta, ja Jukaran
seura/jaoskäyttäjän on hakemus tämän jälkeen hyväksyttävä.

Jäsenrekisterin jäsenluokkia käytetään seuraavasti:

* "Ainaisjäsen" -jäsenluokkaan kuuluvat henkilöt, joille Jukaran johtokunta on
  myöntänyt ainaisjäsenyyden (ehtona min. 10 vuoden maksettu jäsenyys)

* "Jäsen (vuosi)" -jäsenluokkaan kuuluvat kaikki kyseisen vuoden jäsenmaksun
  maksaneet. Esim. vuonna 2024 jäsenmaksun maksaneet kuuluvat jäsenluokkaan
  "Jäsen 2024". Seurakäyttäjä siirtää henkilön tähän luokkaan kun
  jäsenmaksusuoritus on kirjautunut.

Muut jäsenluokat eivät ole aktiivisessa käytössä.

Jäsenmaksujen ja ilmoittautumisten hallinta tapahtumilla
--------------------------------------------------------

.. plantuml::
    :caption: Ilmoittautuminen Suomisportissa

    @startuml

    actor Jäsen
    participant "Suomisport-verkkopalvelu" as Suomisport
    participant "Liikuntaetu-palvelu" as liikuntaetu
    actor Jäsenvastaava

    Jäsen -> Suomisport: Ilmoittautuu aikidoharjoituksiin\nja maksaa jäsen/harjoitusmaksun
    alt jos ei vielä Jukaran jäsen
        Jäsen -> Suomisport: Täyttää jäsenhakemuksen
        Jäsenvastaava -> Suomisport: Hyväksyy jäsenhakemuksen
    end

    Jäsenvastaava -> Suomisport: Siirtää jäsenrekisterissä\noikeaan jäsenluokkaan

    @enduml

Jukarassa käytetään Suomisportin tapahtumatoimintoa ilmoittautumiseen sekä
jäsen- ja harjoitusmaksujen maksamiseen. Käytännön kokemus on osoittanut, että
ne mahdollistavat aikidoharrastukseen paremmin soveltuvan toimintamallin. Suurin
etu on, että tapahtumaan ilmoittautuva henkilö maksaa osallistumismaksun
ilmoittautumisen yhteydessä. Ryhmien osallistumismaksuista lähtetään lasku
kaikille ryhmän jäsenille, mikä on hankalaa kun aikidojaokseen liittyy uusia
jäseniä ympäri vuoden.

Suomisportiin on luotu tapahtumat jokaiselle mahdolliselle laskutuskaudelle:

- Koko vuosi

- Syys- ja kevätkausi (2 tapahtumaa)

- Kuukausittainen harjoitusmaksu (12 tapahtumaa/vuosi)

Kaikissa tapahtumissa voi ilmoittautuessaan valita oikean hinnan 4 vaihtoehdosta

- Pelkkä harjoitusmaksu

- Alennettu harjoitusmaksu (perhealennus)

- Harjoitusmaksu ja jäsenmaksu (valittava vuoden ensimmäistä kautta maksaessa
  ellei ole ainaisjäsen)

- Alennettu harjoitusmaksu ja jäsenmaksu

Seurakäyttäjät käyvät läpi uudet ilmoittautujat säännöllisesti, tarkistavat että
he ovat maksaneet oikean vaihdoehdon mukaisesti ja siirtävät ilmoittautujan
kyseisen vuoden jäsenluokkaan mikäli hän on maksanut jäsenmaksun.

Jäsen- ja harjoitusmaksujen maksaminen muissa palveluissa
---------------------------------------------------------

.. plantuml::
    :caption: Ilmoittautuminen liikuntaetua käyttäen

    @startuml
    title Ilmoittautuminen liikuntaetua käyttäen

    actor Jäsen
    participant "Suomisport-verkkopalvelu" as Suomisport
    participant "Liikuntaetu-palvelu" as liikuntaetu
    actor Jäsenvastaava
    database "Excel-taulukko" as Excel

    Jäsen -> liikuntaetu: Maksaa harjoittelu/jäsenmaksun
    Jäsen -> Jäsenvastaava: Ilmoittaa maksusta sähköpostilla
    alt jos ei vielä Jukaran jäsen
        Jäsen -> Suomisport: Täyttää jäsenhakemuksen
        Jäsenvastaava -> Suomisport: Hyväksyy jäsenhakemuksen
    end

    Jäsenvastaava -> Excel: Kirjaa maksusuorituksen
    Jäsenvastaava -> Suomisport: Siirtää jäsenrekisterissä oikeaan jäsenluokkaan
    Jäsenvastaava -> Suomisport: Lisää jäsenen maksettua kautta vastaavaan tapahtumaan
    @enduml


Monet Jukaran jäsenet maksavat harjoittelunsa työnantajan tarjoamaa liikunta/kulttuurietua (esim. Smartum, ePassi tms.)käyttäen.
Suomisport ei tue tätä, joten maksujen käsittely tapahtuu seuraavasti:

1. Harjoittelija maksaa vaaditun summan käyttämässään palvelussa Jukaralle ja
   ilmoittaa tästä aikidojaoksen jäsenvastaavalle.

2. Aikidojaoksen jäsenvastaava kirjaa tiedon maksusta tarkoitusta varten varten ylläpidettyyn
   Excel-taulukkoon. Taulukkoon on kirjattava
   * maksupäivä
   * henkilön nimi
   * henkilön Suomisportin sportti-id
   * tieto käytetystä palvelusta
   * sisältääkö maksusuoritus Jukaran jäsenmaksun
   * mikä harjoituskausi on maksettu

3. Aikidojaoksen jäsenvastaava lisää ilmoittautuneen henkilön maksettua
   harjoituskautta vastaavaan tapahtumaan ja siirtää hänet kuluvan vuoden
   jäsenluokkaan, jos jäsenmaksu on maksettu samassa suorituksessa. Jokaiseen
   tapahtumaan on luotu jäsenvastaavan tiedossa oleva alennuskoodi, jolla
   tapahtumaan voi lisätä osallistujia ilman, että Suomisportissa tarvitsee tehdä
   uutta maksua.

   Mikäli henkilö ei ole ennestään Jukaran jäsen, pyytää jäsenvastaava häntä
   tekemään jäsenhakemuksen. Tämä on hyväksyttävä ennen kuin muita tämän kohdan
   toimenpiteitä voidaan suorittaa.

4. Jukaran taloudenhoitaja tilittää likuntaetupalveluiden kautta tulleet maksut
   säännöllisesti aikidojaoksen tilille kohdassa 2 mainitun Excel-taulukon
   mukaisesti.

Samaa prosessia voidaan soveltaa myös ilmoittautumisten ja maksujen hoitamiseen
muissa erityistapauksissa, joissa maksaminen Suomisportin kautta ei ole
mahdollista.

Raportointi ja tietojen oikeellisuuden tarkastaminen
----------------------------------------------------


.. plantuml::
    :caption: Tiedonkeruu ja raportointi

    @startuml

    actor Jäsenvastaava
    participant "Python-ohjelma" as Ohjelma
    participant "Suomisport-verkkopalvelu" as Suomisport
    database "Excel-taulukko" as Excel

    Jäsenvastaava -> Ohjelma: Käynnistää ohjelman
    Ohjelma -> Excel: Kerää tiedot liikuntaedulla maksaneista
    Ohjelma -> Suomisport: Kerää tiedot jukaran jäsenistä\nja tapahtumiin ilmoittautuneista
    Ohjelma -> Suomisport: Kerää tiedot aikidolisensseista
    Ohjelma -> Raportointi: Tuottaa tilastoja ja raportteja

    @enduml

Suomisportin jäsenrekisteri ja tapahtumaosio ovat pääosin toisistaan erillisiä.
Niiden tietojen yhdenmukaisuuden varmistamiseksi käytetään erillistä
Python-ohjelmaa, joka lukee näiden osoiden tietojen lisäksi
Suomisportista lisenssitiedot sekä liikuntaetupalveluilla tehdyt maksut yllä
kuvatusta Excel-taulukosta. Ohjelma tarkistaa, että samat henkilöt ovat maksaneet
lisenssin, jäsenmaksun ja harjoitusmaksun sekä että heidän on kirjattu oikeisiin
jäsenluokkiin ja raportoi kaikki havaitsemansa poikkeamat.

Samaa ohjelmaa voidaan käyttää myös seuran ja sen sidosryhmien (esim. kaupungin
liikuntatoimi ja Aikidoliitto) tarvitsemien raporttien tuottamiseen.
