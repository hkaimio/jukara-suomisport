..
  SPDX-License-Identifier: CC-BY-4.0
  SPDX-FileCopyrightText: 2024 Harri Kaimio <harri@kaimio.fi>


This repository includes scripts to automate reporting and integrity validation
of `Jukara aikido`_ club member data. The scripts

* Fetch membership and payment data from Suomisport web side using Selenium

* Read information about payments done using other methods (from Excel file)

* Check integrity of the information and report any inconsistencies

* Check if any members have unpaid fees or other actions that they need to perform

Default settings are for Jukara; however, the scripts could be useful for other
clubs using Suomisport service too.

Usage
-----

1. Copy the example configuration file `conf-example.yaml` to `conf.yaml` and
   edit it. You need to add at least your Suomisport user account and password
   and change other configuration to match your club. See comments in the file
   for details.

2. Scrape data from Suomisport using command:

   .. code::

       python .\src\memberinfo.py --scrape --data .\members.pickle --conf .\conf.yaml

   This will read data from Suomisport web site and store a local cached copy
   in file `members.pickle`. It will also report any inconsistencies it finds
   in the data.

   Later, you can rerun the script using the local data by just leaving the
   `--scrape` option out.

3. To include information about payment transactions done outside Suomisport,
   create an Excel file with that information and add option `--ext-payments
   <path to the file>` to the command. See file
   `tests/data/liikuntaetu-2024.xlsx <tests/data/liikuntaetu-2024.xlsx>`_ for an
   example.

4. Further analysis of the data is possible e.g. using Jupyter Notebooks. See
   examples in the `notebooks`_ directory.

Description of how Suomisport is adapted in Jukara can be found `here
<doc/jukara-suomisport.rst>`_ (in Finnish).

.. _Poetry: https://python-poetry.org/
.. _Jukara aikido: https://jukara.fi/aikido/
